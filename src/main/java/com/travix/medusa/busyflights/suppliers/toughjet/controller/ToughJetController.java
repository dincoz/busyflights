package com.travix.medusa.busyflights.suppliers.toughjet.controller;

import com.travix.medusa.busyflights.busyflights.dto.BusyFlightsRequest;
import com.travix.medusa.busyflights.busyflights.dto.BusyFlightsResponse;
import com.travix.medusa.busyflights.suppliers.toughjet.dto.ToughJetRequest;
import com.travix.medusa.busyflights.suppliers.toughjet.dto.ToughJetResponse;
import com.travix.medusa.busyflights.suppliers.toughjet.service.ToughJetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * This is created by Dinco-WORK on 18.01.2018.
 */
@RestController
@RequestMapping("/toughjet")
public class ToughJetController {

    @Autowired
    private ToughJetService toughJetService;

    @RequestMapping(value = "/search", method = RequestMethod.POST, produces = {"application/json"})
    @ResponseBody
    public List<BusyFlightsResponse> search(@RequestBody final BusyFlightsRequest busyFlightsRequest) {
        return toughJetService.getFlightResults(busyFlightsRequest);
    }
}
