package com.travix.medusa.busyflights.suppliers.toughjet.service;

import com.travix.medusa.busyflights.busyflights.dto.BusyFlightsRequest;
import com.travix.medusa.busyflights.busyflights.dto.BusyFlightsResponse;
import com.travix.medusa.busyflights.suppliers.Suppliers;
import com.travix.medusa.busyflights.suppliers.toughjet.dto.ToughJetRequest;
import com.travix.medusa.busyflights.suppliers.toughjet.dto.ToughJetResponse;
import com.travix.medusa.busyflights.suppliers.toughjet.repository.ToughJetStubRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * This is created by Dinco-WORK on 17.01.2018.
 */
@Service
public class ToughJetService {

    @Autowired
    ToughJetStubRepository toughJetStubRepository;

    public List<BusyFlightsResponse> getFlightResults(BusyFlightsRequest busyFlightsRequest){
        List<ToughJetResponse> toughJetResponses = toughJetStubRepository.getFlights(busyFlightsRequest);
        List<BusyFlightsResponse> busyFlightsResponses = toughJetResponses.stream()
                .map(r -> convertResponse(r))
                .collect(Collectors.toList());
        return busyFlightsResponses;
    }

    public BusyFlightsResponse convertResponse(ToughJetResponse toughJetResponse){
        BusyFlightsResponse busyFlightsResponse = new BusyFlightsResponse();
        busyFlightsResponse.setAirline(toughJetResponse.getCarrier());
        busyFlightsResponse.setSupplier(Suppliers.CRAZY_AIR);
        busyFlightsResponse.setFare(
                toughJetResponse.getBasePrice() +
                        toughJetResponse.getTax() -
                        toughJetResponse.getDiscount()
        );
        busyFlightsResponse.setDepartureAirportCode(toughJetResponse.getDepartureAirportName());
        busyFlightsResponse.setDestinationAirportCode(toughJetResponse.getArrivalAirportName());
        busyFlightsResponse.setDepartureDate(toughJetResponse.getOutboundDateTime());
        busyFlightsResponse.setArrivalDate(toughJetResponse.getInboundDateTime());
        return busyFlightsResponse;
    }
}
