package com.travix.medusa.busyflights.suppliers.crazyair.controller;

import com.travix.medusa.busyflights.busyflights.dto.BusyFlightsRequest;
import com.travix.medusa.busyflights.busyflights.dto.BusyFlightsResponse;
import com.travix.medusa.busyflights.suppliers.crazyair.service.CrazyAirService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * This is created by Dinco-WORK on 18.01.2018.
 */
@RestController
@RequestMapping("/crazyair")
public class CrazyAirController {

    @Autowired
    private CrazyAirService crazyAirService;

    @RequestMapping(value = "/search", method = RequestMethod.POST, produces = {"application/json"})
    @ResponseBody
    public List<BusyFlightsResponse> search(@RequestBody final BusyFlightsRequest busyFlightsRequest) {
        return crazyAirService.getFlightResults(busyFlightsRequest);
    }
}
