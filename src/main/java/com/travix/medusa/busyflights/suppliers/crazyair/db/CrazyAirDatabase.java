package com.travix.medusa.busyflights.suppliers.crazyair.db;

import com.travix.medusa.busyflights.suppliers.Suppliers;
import com.travix.medusa.busyflights.suppliers.crazyair.dto.CrazyAirResponse;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * This is created by Dinco-WORK on 18.01.2018.
 */
@Component
public class CrazyAirDatabase {

    public final List<CrazyAirResponse> db = new ArrayList<>();

    public CrazyAirDatabase() {
        CrazyAirResponse crazyAirResponse = new CrazyAirResponse();
        crazyAirResponse.setAirline(Suppliers.CRAZY_AIR);
        crazyAirResponse.setDestinationAirportCode("LHR");
        crazyAirResponse.setDepartureAirportCode("AMS");
        crazyAirResponse.setPrice(100.00);
        crazyAirResponse.setDepartureDate("10.10.2017");
        crazyAirResponse.setArrivalDate("11.10.2017");
        db.add(crazyAirResponse);

        crazyAirResponse = new CrazyAirResponse();
        crazyAirResponse.setAirline(Suppliers.CRAZY_AIR);
        crazyAirResponse.setDestinationAirportCode("AMS");
        crazyAirResponse.setDepartureAirportCode("LHR");
        crazyAirResponse.setPrice(120.00);
        crazyAirResponse.setDepartureDate("11.10.2017");
        crazyAirResponse.setArrivalDate("12.10.2017");
        db.add(crazyAirResponse);

        crazyAirResponse = new CrazyAirResponse();
        crazyAirResponse.setAirline(Suppliers.CRAZY_AIR);
        crazyAirResponse.setDestinationAirportCode("AMS");
        crazyAirResponse.setDepartureAirportCode("SAW");
        crazyAirResponse.setPrice(200.00);
        crazyAirResponse.setDepartureDate("14.10.2017");
        crazyAirResponse.setArrivalDate("19.10.2017");
        db.add(crazyAirResponse);
    }
}
