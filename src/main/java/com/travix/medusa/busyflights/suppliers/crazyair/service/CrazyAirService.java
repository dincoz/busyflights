package com.travix.medusa.busyflights.suppliers.crazyair.service;

import com.travix.medusa.busyflights.busyflights.dto.BusyFlightsRequest;
import com.travix.medusa.busyflights.busyflights.dto.BusyFlightsResponse;
import com.travix.medusa.busyflights.suppliers.Suppliers;
import com.travix.medusa.busyflights.suppliers.crazyair.dto.CrazyAirRequest;
import com.travix.medusa.busyflights.suppliers.crazyair.dto.CrazyAirResponse;
import com.travix.medusa.busyflights.suppliers.crazyair.repository.CrazyAirStubRepository;
import com.travix.medusa.busyflights.suppliers.toughjet.dto.ToughJetResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * This is created by Dinco-WORK on 17.01.2018.
 */
@Service
public class CrazyAirService {

    @Autowired
    CrazyAirStubRepository crazyAirStubRepository;

    public List<BusyFlightsResponse> getFlightResults(BusyFlightsRequest busyFlightsRequest){
        List<CrazyAirResponse> flights = crazyAirStubRepository.getFlights(busyFlightsRequest);
        List<BusyFlightsResponse> busyFlightsResponses = flights.stream()
                .map(r -> convertResponse(r))
                .collect(Collectors.toList());
        return busyFlightsResponses;
    }

    public BusyFlightsResponse convertResponse(CrazyAirResponse crazyAirResponse){
        BusyFlightsResponse busyFlightsResponse = new BusyFlightsResponse();
        busyFlightsResponse.setAirline(crazyAirResponse.getAirline());
        busyFlightsResponse.setSupplier(Suppliers.CRAZY_AIR);
        busyFlightsResponse.setFare(crazyAirResponse.getPrice());
        busyFlightsResponse.setDepartureAirportCode(crazyAirResponse.getDepartureAirportCode());
        busyFlightsResponse.setDestinationAirportCode(crazyAirResponse.getDestinationAirportCode());
        busyFlightsResponse.setDepartureDate(crazyAirResponse.getDepartureDate());
        busyFlightsResponse.setArrivalDate(crazyAirResponse.getArrivalDate());
        return busyFlightsResponse;
    }
}
