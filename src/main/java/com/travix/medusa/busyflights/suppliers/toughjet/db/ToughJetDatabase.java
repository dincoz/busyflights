package com.travix.medusa.busyflights.suppliers.toughjet.db;

import com.travix.medusa.busyflights.suppliers.Suppliers;
import com.travix.medusa.busyflights.suppliers.toughjet.dto.ToughJetResponse;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * This is created by Dinco-WORK on 18.01.2018.
 */
@Component
public class ToughJetDatabase {

    public final List<ToughJetResponse> db = new ArrayList<>();

    public ToughJetDatabase() {
        ToughJetResponse toughJetResponse = new ToughJetResponse();
        toughJetResponse.setCarrier(Suppliers.TOUGH_JET);
        toughJetResponse.setArrivalAirportName("LHR");
        toughJetResponse.setDepartureAirportName("AMS");
        toughJetResponse.setBasePrice(100.00);
        toughJetResponse.setDiscount(10.00);
        toughJetResponse.setTax(5.00);
        toughJetResponse.setInboundDateTime("10.10.2017");
        toughJetResponse.setOutboundDateTime("11.10.2017");
        db.add(toughJetResponse);

        toughJetResponse = new ToughJetResponse();
        toughJetResponse.setCarrier(Suppliers.TOUGH_JET);
        toughJetResponse.setArrivalAirportName("AMS");
        toughJetResponse.setDepartureAirportName("LHR");
        toughJetResponse.setBasePrice(120.00);
        toughJetResponse.setDiscount(10.00);
        toughJetResponse.setTax(5.00);
        toughJetResponse.setInboundDateTime("11.10.2017");
        toughJetResponse.setOutboundDateTime("12.10.2017");
        db.add(toughJetResponse);

        toughJetResponse = new ToughJetResponse();
        toughJetResponse.setCarrier(Suppliers.TOUGH_JET);
        toughJetResponse.setArrivalAirportName("AMS");
        toughJetResponse.setDepartureAirportName("SAW");
        toughJetResponse.setBasePrice(200.00);
        toughJetResponse.setDiscount(10.00);
        toughJetResponse.setTax(5.00);
        toughJetResponse.setInboundDateTime("14.10.2017");
        toughJetResponse.setOutboundDateTime("19.10.2017");
        db.add(toughJetResponse);
    }
}
