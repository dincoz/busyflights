package com.travix.medusa.busyflights.suppliers.toughjet.dto;

import lombok.Data;

@Data
public class ToughJetRequest {
    private String from;
    private String to;
    private String outboundDate;
    private String inboundDate;
    private int numberOfAdults;
}
