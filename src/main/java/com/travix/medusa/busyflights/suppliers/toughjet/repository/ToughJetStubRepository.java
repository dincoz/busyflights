package com.travix.medusa.busyflights.suppliers.toughjet.repository;

import com.travix.medusa.busyflights.busyflights.dto.BusyFlightsRequest;
import com.travix.medusa.busyflights.suppliers.toughjet.db.ToughJetDatabase;
import com.travix.medusa.busyflights.suppliers.toughjet.dto.ToughJetResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * This is created by Dinco-WORK on 18.01.2018.
 */
@Component
public class ToughJetStubRepository {
    @Autowired
    ToughJetDatabase toughJetDatabase;

    public List<ToughJetResponse> getFlights(BusyFlightsRequest busyFlightsRequest){
        List<ToughJetResponse> response = toughJetDatabase.db.stream()
                .filter(i -> i.getInboundDateTime().equals(busyFlightsRequest.getDepartureDate()))
                .filter(i -> i.getOutboundDateTime().equals(busyFlightsRequest.getReturnDate()))
                .filter(i -> i.getDepartureAirportName().equals(busyFlightsRequest.getOrigin()))
                .filter(i -> i.getArrivalAirportName().equals(busyFlightsRequest.getDestination()))
                .collect(Collectors.toList());
        return response;
    }
}
