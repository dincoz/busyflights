package com.travix.medusa.busyflights.suppliers.crazyair.repository;

import com.travix.medusa.busyflights.busyflights.dto.BusyFlightsRequest;
import com.travix.medusa.busyflights.suppliers.crazyair.db.CrazyAirDatabase;
import com.travix.medusa.busyflights.suppliers.crazyair.dto.CrazyAirResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * This is created by Dinco-WORK on 18.01.2018.
 */
@Component
public class CrazyAirStubRepository {
    @Autowired
    CrazyAirDatabase crazyAirDatabase;

    public List<CrazyAirResponse> getFlights(BusyFlightsRequest busyFlightsRequest){
        List<CrazyAirResponse> response = crazyAirDatabase.db.stream()
                .filter(i -> i.getDepartureDate().equals(busyFlightsRequest.getDepartureDate()))
                .filter(i -> i.getArrivalDate().equals(busyFlightsRequest.getReturnDate()))
                .filter(i -> i.getDepartureAirportCode().equals(busyFlightsRequest.getOrigin()))
                .filter(i -> i.getDestinationAirportCode().equals(busyFlightsRequest.getDestination()))
                .collect(Collectors.toList());
        return response;
    }
}
