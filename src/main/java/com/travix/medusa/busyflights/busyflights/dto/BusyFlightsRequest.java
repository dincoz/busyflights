package com.travix.medusa.busyflights.busyflights.dto;

import lombok.Data;

@Data
public class BusyFlightsRequest {
    private String origin;
    private String destination;
    private String departureDate;
    private String returnDate;
    private int numberOfPassengers;
}
