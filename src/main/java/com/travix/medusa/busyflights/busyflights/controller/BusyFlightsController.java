package com.travix.medusa.busyflights.busyflights.controller;

import com.travix.medusa.busyflights.busyflights.dto.BusyFlightsRequest;
import com.travix.medusa.busyflights.busyflights.dto.BusyFlightsResponse;
import com.travix.medusa.busyflights.busyflights.service.BusyFlightsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * This is created by Dinco-WORK on 18.01.2018.
 */
@RestController
@RequestMapping("/busyflights")
public class BusyFlightsController {

    @Autowired
    private BusyFlightsService busyFlightsService;

    @RequestMapping(value = "/search", method = RequestMethod.POST, produces = {"application/json"})
    @ResponseBody
    public List<BusyFlightsResponse> search(@RequestBody final BusyFlightsRequest busyFlightsRequest) {
        return busyFlightsService.searchFlights(busyFlightsRequest);
    }

}
