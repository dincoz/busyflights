package com.travix.medusa.busyflights.busyflights.service;

import com.travix.medusa.busyflights.busyflights.dto.BusyFlightsRequest;
import com.travix.medusa.busyflights.busyflights.dto.BusyFlightsResponse;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * This is created by Dinco-WORK on 18.01.2018.
 */
public abstract class SupplierCallerService {

    public List<BusyFlightsResponse> call(BusyFlightsRequest busyFlightsRequest) {
        final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.postForObject(getEndpoint(), busyFlightsRequest, List.class);
    }

    protected abstract String getEndpoint();
}
