package com.travix.medusa.busyflights.busyflights.service;

import com.travix.medusa.busyflights.busyflights.dto.BusyFlightsRequest;
import com.travix.medusa.busyflights.busyflights.dto.BusyFlightsResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * This is created by Dinco-WORK on 18.01.2018.
 */
@Service
public class CrazyAirApiService extends SupplierCallerService{

    @Value("${supplier.crazyair.endpoint}")
    private String endpoint;

    public List<BusyFlightsResponse> searchFlights(BusyFlightsRequest busyFlightsRequest){
        return call(busyFlightsRequest);
    }

    @Override
    protected String getEndpoint() {
        return endpoint;
    }
}
