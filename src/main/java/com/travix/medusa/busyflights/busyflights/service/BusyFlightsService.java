package com.travix.medusa.busyflights.busyflights.service;

import com.travix.medusa.busyflights.busyflights.dto.BusyFlightsRequest;
import com.travix.medusa.busyflights.busyflights.dto.BusyFlightsResponse;
import com.travix.medusa.busyflights.suppliers.Suppliers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * This is created by Dinco-WORK on 18.01.2018.
 */
@Service
public class BusyFlightsService {

    @Autowired
    CrazyAirApiService crazyAirApiService;

    @Autowired
    ToughJetApiService toughJetApiService;

    public List<BusyFlightsResponse> searchFlights(BusyFlightsRequest busyFlightsRequest){
        List<BusyFlightsResponse> responses = new ArrayList<>();
        responses.addAll(crazyAirApiService.searchFlights(busyFlightsRequest));
        responses.addAll(toughJetApiService.searchFlights(busyFlightsRequest));
        return responses;
    }
}
